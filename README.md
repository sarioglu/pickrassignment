# Pickr Assignment Project

## Requirements

- node (`brew install node`)
- yarn (`npm install -g yarn`)
- watchman (`brew install watchman`)
- cocoapods (`sudo gem install cocoapods`)
- xcode
- xcode Command Line Tools
- JDK (`brew cask install adoptopenjdk/openjdk/adoptopenjdk8`)
- Android Studio
- Android SDK

Please refer to https://reactnative.dev/docs/environment-setup for more info

## Installation

- Use yarn to install required node dependencies using the following command: `yarn install`

## Running

- Use following command to run metro bundler: `yarn start`
- You may use `yarn ios` and `yarn android` to run iOS and Android emulators respectively.
  - You may need to configure emulators before.

## Details

This assignment application uses `redux` to store its application state.
It starts with an initial state and changes it by triggering actions as user presses "GENERATE" button.
In order to store history, a `React Context` listens for the changes in `redux` store.
These changes then provided to a modal which can be opened by pressing "VIEW LOGS" button.

## Known Issues

- Some integration tests can be added.
- Some more configuration is needed to build release version of iOS app.
- End to end test runner detox is not working for now.
