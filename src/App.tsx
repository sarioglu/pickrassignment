import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {Provider} from 'react-redux';

import store from './store';
import RandomBoxesContainer from './components/RandomBoxesContainer';
import {LoggerProvider} from './context/logger';

const App = () => {
  return (
    <Provider store={store}>
      <LoggerProvider>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <RandomBoxesContainer />
        </SafeAreaView>
      </LoggerProvider>
    </Provider>
  );
};

export default App;
