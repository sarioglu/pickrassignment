import React, {FC, useState} from 'react';
import {FlatList, Button, ListRenderItem, StyleSheet, View} from 'react-native';

import Box from './Box';

import {
  RandomBoxesState,
  GenerateRandomBoxesAction,
  RandomBox,
} from '../models/randomBox';
import LogsModal from './LogsModal';

const styles = StyleSheet.create({
  root: {
    height: '100%',
    justifyContent: 'space-between',
  },
  list: {
    margin: 16,
  },
  item: {
    marginVertical: 8,
  },
});

// TODO: Find something other than index to use as key
const ListItem: ListRenderItem<RandomBox> = ({item, index}) => (
  <View style={styles.item} key={index}>
    <Box label={`${item.value}`} backgroundColor={item.backgroundColor} />
  </View>
);

interface Props {
  randomBoxes: RandomBoxesState;
  generateRandomBoxes: () => GenerateRandomBoxesAction;
}

const RandomBoxes: FC<Props> = ({randomBoxes, generateRandomBoxes}) => {
  const [logsVisible, setLogsVisible] = useState(false);

  return (
    <View style={styles.root}>
      <LogsModal
        visible={logsVisible}
        onRequestClose={() => setLogsVisible(false)}
      />
      <FlatList data={randomBoxes} renderItem={ListItem} style={styles.list} />
      <View>
        <Button title="GENERATE" onPress={() => generateRandomBoxes()} />
        <Button title="VIEW LOGS" onPress={() => setLogsVisible(true)} />
      </View>
    </View>
  );
};

export default RandomBoxes;
