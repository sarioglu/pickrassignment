import React, {FC} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  root: {
    width: '100%',
    height: 120,
    borderRadius: 8,
    backgroundColor: 'darkblue',

    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    color: '#ffffff',
    fontSize: 48,
    fontWeight: 'bold',
  },
});

interface Props {
  label: string;
  backgroundColor: string;
}

const Box: FC<Props> = ({label, backgroundColor}) => (
  <View style={{...styles.root, backgroundColor}}>
    <Text style={styles.label} testID="box-label">
      {label}
    </Text>
  </View>
);

export default Box;
