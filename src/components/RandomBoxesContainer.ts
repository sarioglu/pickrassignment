import {connect} from 'react-redux';

import RandomBoxes from './RandomBoxes';
import {RootState} from '../store/reducers';
import {generateRandomBoxes} from '../store/actions/randomBoxActions';

const mapStateToProps = ({randomBoxes}: RootState) => ({randomBoxes});

const mapDispatchToProps = {
  generateRandomBoxes,
};

const RandomBoxesContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RandomBoxes);

export default RandomBoxesContainer;
