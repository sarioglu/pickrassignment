import React, {FC} from 'react';
import {
  Modal,
  View,
  Text,
  Button,
  StyleSheet,
  FlatList,
  ListRenderItem,
} from 'react-native';
import {RandomBoxesState} from '../models/randomBox';
import {useLogger} from '../context/logger';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  content: {
    paddingHorizontal: 16,
    paddingVertical: 32,
    backgroundColor: '#ffffff',
    height: '50%',
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
  },
  logList: {
    flex: 1,
    marginVertical: 32,
  },
  item: {
    paddingVertical: 4,
    paddingHorizontal: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemValue: {
    fontSize: 24,
  },
  emptyView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  emptyMessage: {
    fontSize: 24,
  },
});

// TODO: Find something other than index to use as key
const ListItem: ListRenderItem<RandomBoxesState> = ({item, index}) => (
  <View key={index} style={styles.item}>
    {item.map((randomBox, i) => (
      <Text key={i} style={styles.itemValue}>
        {randomBox.value}
      </Text>
    ))}
  </View>
);

const EmptyState: FC = () => (
  <View style={styles.emptyView}>
    <Text style={styles.emptyMessage}>List is empty</Text>
  </View>
);

interface Props {
  visible: boolean;
  onRequestClose: () => void;
}

const LogsModal: FC<Props> = ({visible, onRequestClose}) => {
  const {logs} = useLogger();

  return (
    <Modal
      transparent
      visible={visible}
      animationType="slide"
      onRequestClose={onRequestClose}>
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.title}>Logs</Text>
          <FlatList
            data={logs}
            renderItem={ListItem}
            style={styles.logList}
            ListEmptyComponent={EmptyState}
          />
          <Button title="CLOSE" onPress={onRequestClose} />
        </View>
      </View>
    </Modal>
  );
};

export default LogsModal;
