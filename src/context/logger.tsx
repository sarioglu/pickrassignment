import React, {FC, createContext, useContext, useState} from 'react';

import {RandomBoxesState} from '../models/randomBox';
import {useStore} from 'react-redux';
import {RootState} from '../store/reducers';

export const LoggerContext = createContext({
  logs: [] as RandomBoxesState[],
});

export const LoggerProvider: FC<unknown> = (props) => {
  const store = useStore<RootState>();

  const [logs, setLogs] = useState<RandomBoxesState[]>([
    store.getState().randomBoxes,
  ]);

  store.subscribe(() => {
    setLogs([...logs, store.getState().randomBoxes]);
  });

  return <LoggerContext.Provider value={{logs}} {...props} />;
};

export const useLogger = () => useContext(LoggerContext);
