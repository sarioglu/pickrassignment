export type RandomBox = {value: number; backgroundColor: string};

export type RandomBoxesState = RandomBox[];

export enum ACTION_TYPES {
  'GENERATE' = 'RANDOM_BOXES/GENERATE',
}

export type GenerateRandomBoxesAction = {
  type: ACTION_TYPES.GENERATE;
};

export type RandomBoxAction = GenerateRandomBoxesAction;
