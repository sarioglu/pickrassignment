import {combineReducers} from 'redux';

import randomBoxes from './randomBoxesReducer';

const rootReducer = combineReducers({randomBoxes});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
