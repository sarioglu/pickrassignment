import {Reducer} from 'redux';

import {
  RandomBoxesState,
  RandomBoxAction,
  ACTION_TYPES,
} from '../../models/randomBox';

const BOX_COUNT = 3;

const getRandomDigit = () => Math.floor(Math.random() * 10);
const getRandomColor = () =>
  `hsl(${Math.floor(Math.random() * 360)}, ${Math.floor(
    Math.random() * 50,
  )}%, ${50 + Math.floor(Math.random() * 30)}%)`;

const getRandomBox = () => ({
  value: getRandomDigit(),
  backgroundColor: getRandomColor(),
});

const initialState: RandomBoxesState = Array.from(
  {length: BOX_COUNT},
  getRandomBox,
);

const randomBoxesReducer: Reducer<RandomBoxesState> = (
  state = initialState,
  action: RandomBoxAction,
) => {
  switch (action.type) {
    case ACTION_TYPES.GENERATE:
      return Array.from({length: BOX_COUNT}, getRandomBox);
    default:
      return state;
  }
};

export default randomBoxesReducer;
