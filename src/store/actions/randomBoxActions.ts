import {ActionCreator} from 'redux';
import {GenerateRandomBoxesAction, ACTION_TYPES} from '../../models/randomBox';

export const generateRandomBoxes: ActionCreator<GenerateRandomBoxesAction> = () => ({
  type: ACTION_TYPES.GENERATE,
});
