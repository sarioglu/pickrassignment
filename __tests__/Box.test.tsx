import React from 'react';
import {render} from '@testing-library/react-native';

import Box from '../src/components/Box';

describe('Box component', () => {
  jest.useFakeTimers();

  it('should render properly', () => {
    expect(
      render(<Box label="1" backgroundColor="black" />).asJSON(),
    ).toMatchSnapshot();
  });
});
