import store from '../src/store';
import {generateRandomBoxes} from '../src/store/actions/randomBoxActions';

describe('Redux store', () => {
  it('should have correct initial state', () => {
    expect(store.getState()).toMatchSnapshot({
      randomBoxes: expect.arrayContaining([
        {
          value: expect.any(Number),
          backgroundColor: expect.any(String),
        },
      ]),
    });
  });

  it('should handle GENERATE action', () => {
    store.dispatch(generateRandomBoxes());
    expect(store.getState()).toMatchSnapshot({
      randomBoxes: expect.arrayContaining([
        {
          value: expect.any(Number),
          backgroundColor: expect.any(String),
        },
      ]),
    });
  });
});
